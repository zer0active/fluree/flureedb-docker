FROM openjdk:8 AS FlureeDB
LABEL Description="FlureeDB Community Edition - Docker Image"
LABEL version="1.0"
LABEL maintainer="allan.tomkinson@gmail.com"
ADD flureedb/* /usr/local/flureedb/

ENV fdb-group-private-key-file "/opt/flureedb/default-private-key.txt"
VOLUME /opt/flureedb /opt/flureedb

#ARG username
#ARG password

ENTRYPOINT ["/usr/local/flureedb/flureeDB_transactor.sh"]
EXPOSE 8080
EXPOSE 9790
