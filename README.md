# flureedb-docker

# DEPRECATED!  This project has been deprecated in favor of the Fluree-Docker-Example project hosted on GitHub by Fluree, PBC at https://github.com/fluree/fluree-docker-example  Please use that project instead of this one.

## Docker Container image build for FlureeDB

**Warning: This project is not ready for production use!**

Things to be aware of: 
- It currently uses the default private key file that FlureeDB generates when it starts up.  See the property file for the location of this key in the container.  
- It currently uses the default username / password assigned by Fluree.
- It exposes the HTTP port (8080) to the container host machine.  
- It exposes the Consensus port (9790) to the container network, but not to the host machine.

Note: This project does not include the FlureeDB distribution.  To download FlureeDB, visit https://flur.ee

## First time build steps

1. Clone / download the files from this project.

2. Create a "flureedb" directory in the same directory as the files for this project.

2. Download FlureeDB from https://flur.ee 

3. From the downloaded archive, copy the files in the "flureeDB-#.#.#" directory place those files inside of the "flureedb" folder you created above in step 2.

4. Run "docker-compose build"

5. Run "docker-compose run"

If everything went right, you should be able to hit "http://localhost:8080/login" on the container host machine to get to the FlureeDB login screen.  

To learn what to do with FlureeDB from this point, visit https://docs.flur.ee


